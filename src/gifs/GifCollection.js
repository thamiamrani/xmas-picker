import rein from "./reindeer.gif";
import bart from "./bart.gif";
import office from "./office.gif";
import lit from "./lit.gif";

export const imgData = [
  {
    img: "https://media3.giphy.com/media/ZR0wpd59SBvry/giphy.gif?cid=790b7611d3c440db8bbcee752ef7a9a2dd0bffeb15f9cb19&rid=giphy.gif",
    title: "Image",
    cols: 4.8
  },
  {
    img: rein,
    title: "Image",
    cols: 4.8
  },
  {
    img: "https://media2.giphy.com/media/3o6fJ1UDfEBc9BcPss/giphy.gif?cid=790b761147bf492ccf58c2f6dc566e62e5d67db5b48d1878&rid=giphy.gif",
    title: "Image",
    cols: 2.4
  },
  {
    img: bart,
    title: "Image",
    cols: 4
  },
  {
    img: "https://media0.giphy.com/media/V39CvtftZIveg/giphy.gif?cid=790b7611d404bfd4acf067662c5c7654b97308eae7945ec7&rid=giphy.gif",
    title: "Image",
    cols: 4
  },
  {
    img: office,
    title: "Image",
    cols: 4
  },
  {
    img: lit,
    title: "Image",
    cols: 5.6
  },
  {
    img: "https://media2.giphy.com/media/1mw7lqldpZtOE/giphy.gif?cid=790b7611738fa01fd039aaa52dbed1f89f242ce8f966a1df&rid=giphy.gif",
    title: "Image",
    cols: 6.4
  },

  {
    img: "https://media1.giphy.com/media/jsTbK3IjFF6qQ/giphy.gif?cid=790b7611904c4c7427537f2001e1a70734426a833f79f8bb&rid=giphy.gif",
    title: "Image",
    cols: 12
  }
];
