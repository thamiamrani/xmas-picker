import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import XmasGrid from "./XmasGrid";
import { Vault } from "./Vault";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isSubmited: false, isLogged: false };
    this.setLoggingStatus = this.setLoggingStatus.bind(this);
    this.setSubmitStatus = this.setSubmitStatus.bind(this);
  }
  setLoggingStatus() {
    this.setState({ isLogged: !this.state.isLogged });
  }

  setSubmitStatus() {
    this.setState({ isSubmited: !this.state.isSubmited });
  }

  getLogIn() {
    return this.state.isLogged ? <XmasGrid Submited={this.setSubmitStatus} /> : <Vault Logging={this.setLoggingStatus} />;
  }

  render() {
    return <div>{this.state.isSubmited ? <p>merci, bye.</p> : this.getLogIn()}</div>;
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
