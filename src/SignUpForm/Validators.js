/* eslint-disable no-useless-escape */
const noError = () => [false, ""];
const error = message => [true, message];

const acceptedFirstNames = [
  "Mazine3.25%",
  "BouSmou",
  "Annalbel",
  "Mère du fêté",
  "Chantal avec un e",
  "Eli",
  "Victime de Leo",
  "Yellow B",
  "S'il vous plaît Monsieur",
  "Vous êtes qui vous Monsieur",
  "Thami"
];

export function isFirstNameError(value) {
  const isDefaultError = isDefaultValueError(value);
  if (isDefaultError[0]) {
    return isDefaultError;
  }
  if (caseVerificator(value, "MAZINE")) {
    return error("Yo pas que je veux t'imposer un surnom ni rien, cependant Mazine3.25% est un nice nom.[Mazine3.25%]");
  }
  if (caseVerificator(value, "ESMA")) {
    return error("Esma is not okay with me, tu es une bou smou collé en PascalCase.[BouSmou]");
  }
  if (caseVerificator(value, "ANNABEL")) {
    return error("Sup pornStar name only, sorry.[Annalbel]");
  }
  if (caseVerificator(value, "MARIE")) {
    return error("YO HOE HOE HOE, C'est la fête de ton fils, [Mère du fêté]");
  }
  if (caseVerificator(value, "CHANTALE")) {
    return error("Bien important de pas oublié le e, [Chantal avec un e]");
  }
  if (caseVerificator(value, "ELISABETH")) {
    return error("Trop de manière diffèrente d'écrire ton nom je vais accepeter [Eli]");
  }
  if (caseVerificator(value, "TANIA")) {
    return error("Yo cheveux long, j'accepte [Victime de Leo]");
  }
  if (caseVerificator(value, "KO")) {
    return error("Wuddup IBM ko is too short, try [Yellow B]");
  }
  if (caseVerificator(value, "MARIA")) {
    return error("Question de te respecter dans ta culture le seul nom acceptéest [S'il vous plaît Monsieur]");
  }
  if (caseVerificator(value, "CARLOS")) {
    return error("Hey Pedro ça te tente tu de me dire [Vous êtes qui vous Monsieur]");
  }
  if (!acceptedFirstNames.includes(value)) {
    return error("Je ne vous connais pas, vous ne pouvez participer ceci est un évennement privé.");
  }

  return noError();
}

export function isEmailError(value) {
  const emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const isDefaultError = isDefaultValueError(value);
  if (isDefaultError[0]) {
    return isDefaultError;
  }
  if (!emailRegEx.test(value)) {
    return error("Ceci n'est pas un format de courriel valide!");
  }
  return noError();
}

export function isPasswordError(value, realPassword) {
  if (value !== realPassword) {
    return error("Mauvais mot de passe, essaie le bon ça marchera peut-être");
  }
  return noError();
}

export function isDefaultValueError(value) {
  if (value === "") {
    return error("Ceci ne devrait pas être vide");
  }
  if (value.length <= 1) {
    return error("ceci devrait être plus long");
  }
  return noError();
}

function caseVerificator(value, name) {
  return value.toUpperCase() === name;
}
