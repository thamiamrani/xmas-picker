import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { isFirstNameError, isEmailError, isDefaultValueError } from "./Validators";
import FireBaseClient from "../Db/fireBaseClient";

export default props => {
  const [firstName, setFirstName] = React.useState("");
  const [lastName, setLastName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [interest, setInterest] = React.useState("");

  const [firstNameError, setFirstNameError] = React.useState([undefined, ""]);
  const [lastNameError, setLastNameError] = React.useState([undefined, ""]);
  const [emailError, setEmailError] = React.useState([undefined, ""]);
  const [interestError, setInterestError] = React.useState([undefined, ""]);

  function handleSubmit() {
    errorValidation();
    if (validateSubmission()) {
      FireBaseClient.writeEntry(firstName, lastName, email, interest);
      props.Submited();
    }
  }

  return (
    <div className="paper">
      <form className="form" noValidate>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="firstName"
          label="First Name"
          name="firstName"
          autoComplete="given-name"
          error={firstNameError[0]}
          helperText={firstNameError[0] && firstNameError[1]}
          onChange={e => setFirstName(e.target.value)}
          onBlur={() => setFirstNameError(isFirstNameError(firstName))}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="lastName"
          label="Last Name"
          id="lastName"
          autoComplete="family-name"
          error={lastNameError[0]}
          helperText={lastNameError[0] && lastNameError[1]}
          onChange={e => {
            setLastName(e.target.value);
          }}
          onBlur={() => setLastNameError(isDefaultValueError(lastName))}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email1"
          label="Email Address"
          name="email"
          autoComplete="email"
          error={emailError[0]}
          helperText={emailError[0] && emailError[1]}
          onChange={e => {
            setEmail(e.target.value);
          }}
          onBlur={() => {
            setEmailError(isEmailError(email));
          }}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          multiline
          fullWidth
          name="Interest"
          label="Interests"
          id="Interests"
          placeholder="#1&#10;#2&#10;#3"
          error={interestError[0]}
          helperText={interestError[0] && interestError[1]}
          onChange={e => {
            setInterest(e.target.value);
          }}
          onBlur={() => setInterestError(isDefaultValueError(interest))}
        />
      </form>
      <Button fullWidth variant="contained" color="secondary" className="submit" onClick={handleSubmit}>
        Submit
      </Button>
    </div>
  );

  function validateSubmission() {
    if (firstNameError[0] === false && lastNameError[0] === false && emailError[0] === false && interestError[0] === false) {
      return true;
    }
    return false;
  }

  function errorValidation() {
    setFirstNameError(isFirstNameError(firstName));
    setLastNameError(isDefaultValueError(lastName));
    setEmailError(isEmailError(email));
    setInterestError(isDefaultValueError(interest));
  }
};
