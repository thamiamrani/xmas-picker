/* eslint-disable jsx-a11y/accessible-emoji */
import React from "react";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import CssBaseline from "@material-ui/core/CssBaseline";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Grid from "@material-ui/core/Grid";
import Hidden from "@material-ui/core/Hidden";
import { imgData } from "./gifs/GifCollection";
import "./index.css";
import { SignUpForm } from "./SignUpForm";
import { ListItemText } from "@material-ui/core";

function XmasGrid(props) {
  return (
    <Grid container component="main" className="root">
      <CssBaseline />
      <Hidden mdDown>
        <Grid item sm={12} md={7} className="image">
          <GridList cellHeight={400} cols={12} spacing={0}>
            {imgData.map(image => (
              <GridListTile key={image.img} cols={image.cols || 1}>
                <img src={image.img} alt={image.title} />
              </GridListTile>
            ))}
          </GridList>
        </Grid>
      </Hidden>
      <Hidden mdUp>
        <Grid item sm={7} md={7} className="image">
          <GridList cellHeight={265} cols={12} spacing={0}>
            {imgData.map(image => (
              <GridListTile key={image.img} cols={image.cols || 1}>
                <img src={image.img} alt={image.title} />
              </GridListTile>
            ))}
          </GridList>
        </Grid>
      </Hidden>
      <Grid item sm={12} md={5}>
        <Hidden smDown>
          <img
            src="http://files.lewispr.com/infographics/splunk-12-days-of-christmas/v5-iframe/images/christmas-lights.gif?crc=4263798373"
            alt=""
            width={"100%"}
          ></img>
        </Hidden>
        <Hidden smUp>
          <img
            src="http://files.lewispr.com/infographics/splunk-12-days-of-christmas/v5-iframe/images/christmas-lights.gif?crc=4263798373"
            alt=""
            width={"584px"}
          ></img>
        </Hidden>
        <h1> 🎄🎁 Joyeux Noël - T ki ? 🎁🎄</h1>
        <List id="list">
          <ListItemText primary="Quelques directives: "></ListItemText>
          <ListItem>- Budget 30$ CAD.</ListItem>
          <ListItem>- Tous les champs doivent être rempli.</ListItem>
          <ListItem>- Votre [Prènom] est obligatoire.</ListItem>
          <ListItem>- Dans intérêt il faudrait mettre 2-3 champs d'intérêt (ex : Plantes, Bricolage, Harry Potter). </ListItem>
          <ListItem>
            - Dans intérêt il faudrait aussi mettre votre taille de chandail/chaussette pour aggrandir les possibiltés de cadeaux potentiel.
          </ListItem>
        </List>
        <SignUpForm Submited={props.Submited} />
      </Grid>
    </Grid>
  );
}

export default XmasGrid;
