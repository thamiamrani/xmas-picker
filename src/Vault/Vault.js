import React from "react";
import { TextField, Button } from "@material-ui/core";
import Countdown from "react-countdown-now";
import leo from "../gifs/leo.jpg";
import { isPasswordError } from "../SignUpForm/Validators";
import FireBaseClient from "../Db/fireBaseClient";

export default props => {
  const [password, setPassword] = React.useState("");
  const [passwordError, setPasswordError] = React.useState([false, ""]);

  function passwordValidation(dbPassword) {
    if (password === dbPassword) {
      props.Logging();
    } else {
      setPasswordError(isPasswordError(password, dbPassword));
    }
  }

  const handleSubmit = () => {
    FireBaseClient.readPassword().then(value => {
      passwordValidation(value.val());
    });
  };

  const onKeyDown = event => {
    if (event.keyCode === 13) {
      handleSubmit();
    }
  };

  return (
    <div id="password">
      <img src={leo} alt="leo" id="leo"></img>
      <div id="countdown">
        <Countdown date={new Date("2019/12/28")} />
      </div>
      <TextField
        fullWidth
        variant="outlined"
        placeholder={"password"}
        label="password"
        autoComplete="password"
        type={"password"}
        error={passwordError[0]}
        helperText={passwordError[0] && passwordError[1]}
        onChange={e => {
          setPassword(e.target.value);
        }}
        onKeyDown={onKeyDown}
        autoFocus
      ></TextField>
      <Button fullWidth variant="contained" id="accessBtn" onClick={handleSubmit}>
        Accèder à Noël
      </Button>
    </div>
  );
};
