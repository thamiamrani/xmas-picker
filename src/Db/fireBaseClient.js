import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDpt5DWwxfb0g4htj99o02yIJe5nfY6ykM",
  authDomain: "xmas-picker.firebaseapp.com",
  databaseURL: "https://xmas-picker.firebaseio.com",
  projectId: "xmas-picker",
  storageBucket: "xmas-picker.appspot.com",
  messagingSenderId: "228220028951",
  appId: "1:228220028951:web:253c614a68b7f328b8f452",
  measurementId: "G-LJZ87PQ8FM"
};
firebase.initializeApp(firebaseConfig);

class FireBaseClient {
  writeEntry(FirstName, LastName, Email, Interests) {
    firebase
      .database()
      .ref("users/" + uuidv4())
      .set({
        firstName: FirstName,
        lastName: LastName,
        email: Email,
        interests: Interests
      });
  }

  readPassword() {
    return firebase
      .database()
      .ref("password")
      .once("value");
  }
}

function uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c === "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

export default new FireBaseClient();
